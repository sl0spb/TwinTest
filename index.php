<?php

use App\Model\Sequence;
use App\Service\Logger\FileLogger;

require_once './vendor/autoload.php';

$logger = new FileLogger('var/sequence.log');

$n = 20;
$m = 10;
$sequence = new Sequence($logger, $m);

$digits = static function (int $length) {
    while ($length > 0) {
        yield mt_rand();
        --$length;
    }
};


foreach ($digits($n) as $number) {
    $sequence->add($number);
}

print_r($sequence->getMaxNumbers());

