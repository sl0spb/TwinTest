<?php


namespace App\Service\Logger;


class FileLogger implements LoggerInterface
{
    private $file;

    /**
     * FileLogger constructor.
     *
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    public function log($message): void
    {
        file_put_contents($this->file, (new \DateTime('now'))->format('y:m:d h:i:s'). ' '.$message.PHP_EOL, FILE_APPEND);
    }
}