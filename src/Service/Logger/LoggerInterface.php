<?php

namespace App\Service\Logger;

interface LoggerInterface
{
    public function log($message);
}
