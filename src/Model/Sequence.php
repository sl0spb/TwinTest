<?php

namespace App\Model;

use App\Service\Logger\LoggerInterface;
use SplMaxHeap;
use SplMinHeap;

class Sequence
{
    private $sortedArray;
    private $maxNumbers;
    private $logger;

    public function __construct(LoggerInterface $logger, int $maxNumbers)
    {
        $this->maxNumbers = $maxNumbers;
        $this->logger = $logger;
        $this->sortedArray = new SplMinHeap();

        $this->logger->log('Create a new Sequence object.');
    }

    /**
     * @param int $number
     */
    public function add(int $number): void
    {
        if ($this->sortedArray->count() < $this->maxNumbers) {
            $this->sortedArray->insert($number);
            $this->logger->log('Add ' . $number . ' into the sorted array.');
        } else {
            $top = $this->sortedArray->top();
            if ($number > $top) {
                $this->sortedArray->insert($number);
                $this->sortedArray->extract();
                $this->logger->log('Add ' . $number . ' into the sorted array.');
            }
        }

    }

    public function getMaxNumbers(): array
    {
        $result = [];
        for ($i = $this->maxNumbers - 1; $i >= 0; $i--) {
            $result[$i] = $this->sortedArray->extract();
        }
        return $result;
    }

}